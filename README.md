# Telegram Notify

Monitor your IT environment and be automatically notified by Telegram of events.
The tool is made to work on most Linux system with lightweight installation. Is try to mostly used busybox tools.

## Update
Run `update.sh` to update the software with last modifications. However, it don't run setup and don't activate new functionalities.
Warning : This operation overwrite any modifications made inside the files. You can use `--keep <file1>[,<file2>,...]` or `--diff <files><file1>[,<file2>,...]` to, respectively, keep or check difference of some files. No support is garanti. [AVAILABLE SOON]

Automatic updates are made by cron jobs.
