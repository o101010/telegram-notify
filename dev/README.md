# Documentation for contributors

## Initiate git repo
```
git clone git@gitlab.com:o101010/telegram-notify.git
```
# TODO add info for hook versioning

## Versioning
The version of this project is base on a -major.minor.revision- model with only numeric value. The revision value is automatically increased by git server hook after a push.
The minor or major version need to be manually change if needed.
