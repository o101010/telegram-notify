#!/bin/sh

configFile=$(dirname "$0")"/telegram_config"
if [ -f "${configFile}" ]; then
    groupId=$(awk '/^groupId/{print $3}' "${configFile}")
    botToken=$(awk '/^botToken/{print $3}' "${configFile}")
else
    echo "ERROR : Config file '${configFile}' not found"
    exit 1
fi

# TODO check if the keys are not empty

# Parse the argument of the script
if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  echo "Usage: `basename $0` \"text message\""
  exit 0
fi

if [ -z "$1" ]
  then
    echo "Add message text as second arguments"
    exit 0
fi

if [ "$#" -ne 1 ]; then
    echo "You can pass only one argument. For string with spaces put it on quotes"
    exit 0
fi

bodyData='{"chat_id": "'${groupId}'", "text": "'${1}'"}'
ret=$(wget -qO - --post-data="${bodyData}" --header=Content-Type:application/json "https://api.telegram.org/bot${botToken}/sendMessage")
wgetExitCode=$?
if [ $wgetExitCode -ne 0 ]; then
  echo "ERROR : Unable to send data through API with wget"
  exit $wgetExitCode
fi
echo $ret
# TODO check if curl ok
