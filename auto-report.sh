#!/bin/sh

messageInterval="week"
lastboot="NEVER"
lastUpdate="Useless"
scriptVersion=$(cat $(dirname $0)/VERSION)
if [ $? -ne 0 ]; then
    scriptVersion="0.0.0"
fi

message="Hey, This is a new ${messageInterval}.
last reboot was ${lastReboot}
last updates were ${lastUpdate}

v${scriptVersion}"


$(dirname $0)/telegram-send.sh "$message"
