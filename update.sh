#!/bin/sh

gitBranch="master"

installationFolder=$(dirname $0)
projectUri="https://gitlab.com/o101010/telegram-notify/-/archive/${gitBranch}/telegram-notify-master.tar.gz"
extension=$(echo ${projectUri} | rev | cut -d "." -f 1,2 | rev)
foldername=$(echo ${projectUri} | rev | cut -d "/" -f 1 | rev)
foldername=$(echo ${foldername} | sed "s/.${extension}//g")

wget -q -O - "${projectUri}" | tar -xf -C /tmp/

oldVersion=$(cat ${installationFolder}/VERSION)
if [ $? -ne 0 ]; then
    oldVersion="0.0.0"
fi

newVersion=$(wget -qO - --post-data="${bodyData}" --header=Content-Type:application/json "https://api.telegram.org/bot${botToken}/sendMessage"))
if [ $? -ne 0 ]; then
    newVersion="0.0.0"
fi

# TODO check if update is needed
cp "/tmp/${foldername}/*"
# TODO set permissions of files

rm -rf "/tmp/${foldername}"

message="\u2699 Telegram-notify updated to version ${newVersion} \u2699"

"${installationFolder}/telegram-send.sh" "$message"

# TODO add --keep and --diff support. Remove the note in README after that
