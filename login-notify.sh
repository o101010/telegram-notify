#!/bin/sh
    
# prepare any message you want
login_ip="$(echo $SSH_CONNECTION | cut -d " " -f 1)"
login_date="$(date +"%a %e %B %Y, %R")"
login_name="$(whoami)"

# For new line I use $'\n' here
message="New login to server '"$(hostname)"' :
\ud83e\uddd2 ${login_name}
\ud83d\udda5 ${login_ip}
\ud83d\udcc5 ${login_date}"

#send it to telegram
$(dirname $0)/telegram-send.sh "$message"
